// returns first element selected - $('input[name="food"]')
const $ = document.querySelector.bind(document);
// return array of selected elements - $$('img.dog')
const $$ = document.querySelectorAll.bind(document);

includeHTML();

function get(url, success) {
  var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
  xhr.open('GET', url);
  xhr.onreadystatechange = function() {
    if (xhr.readyState>3 && xhr.status==200) success(xhr.responseText);
  };
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.send();
  return xhr;
}

function postAjax(url, data, success) {
  var params = typeof data == 'string' ? data : Object.keys(data).map(
    function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
  ).join('&');

  var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
  xhr.open('POST', url);
  xhr.onreadystatechange = function() {
    if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText); }
  };
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.send(params);
  return xhr;
}


function listFilter_1(input,listid) {
  var filter, ul, li, a, i, txtValue;
  filter = input.value.toUpperCase();
  ul = document.getElementById(listid);
  li = ul.getElementsByTagName("li");
  for (i = 0; i < li.length; i++) {
    a = li[i].getElementsByTagName("a")[0];
    txtValue = a.textContent || a.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
    } else {
      li[i].style.display = "none";
    }
  }
}

function createFilter_1(output_box_id,input_id,list_id,items,place_holder){
  let output_box = document.querySelector(`#${output_box_id}`);
  output_box.innerHTML = `
    <input type="text" id="${input_id}" class="filterInput" onkeyup="listFilter_1(this,'${list_id}')" placeholder="${place_holder}">

    <ul id="${list_id}" class="filterList">
    </ul>`

  let list = document.querySelector(`#${list_id}`);
  for( i of items ){
    list.innerHTML += `<li class="listItem"><a href="#">${i}</a></li>`;
  }
}
// Get the modal

function showModal(title,msg,footer) {
  let modal = document.getElementById("myModal");
  document.getElementById("modalTitle").innerHTML=title;
  document.getElementById("modalMsg").innerHTML=msg;
  document.getElementById("modalFooter").innerHTML=footer;
  modal.style.display = "block";
}

function hideModal() {
  let modal = document.getElementById("myModal");
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  let modal = document.getElementById("myModal");
  if (event.target == modal) {
    hideModal();
  }
}

document.addEventListener('keydown', function(e) {
  if (e.key === "Escape") {
    hideModal();
  }
});


function includeHTML() {
  var z, i, elmnt, file, xhttp;
  /* Loop through a collection of all HTML elements: */
    z = document.getElementsByTagName("*");
  for (i = 0; i < z.length; i++) {
    elmnt = z[i];
    /*search for elements with a certain atrribute:*/
      file = elmnt.getAttribute("w3-include-html");
    if (file) {
      /* Make an HTTP request using the attribute value as the file name: */
        xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
          if (this.status == 200) {elmnt.innerHTML = this.responseText;}
          if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
          /* Remove the attribute, and call this function once more: */
            elmnt.removeAttribute("w3-include-html");
          includeHTML();
        }
      }
      xhttp.open("GET", file, true);
      xhttp.send();
      /* Exit the function: */
        return;
    }
  }
}
